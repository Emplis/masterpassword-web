mod utils;

use wasm_bindgen::prelude::*;

use masterpassword_rs::mpversion::MPVersion;
use masterpassword_rs::key_scope::KeyScope;
use masterpassword_rs::template_classe::TemplateClasse;

use masterpassword_rs::mpw::calculate_master_key;
use masterpassword_rs::mpw::calculate_site_key;
use masterpassword_rs::mpw::site_password;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

/**
 * Get the corresponding `MPVersion` from the given number
 * 
 * Argument:
 * 
 * - `version` - Version of the Master Password as a number.
 */
fn get_mpversion(version: usize) -> MPVersion {
    match version {
        0 => MPVersion::V0,
        1 => MPVersion::V1,
        2 => MPVersion::V2,
        3 => MPVersion::V3,
        _ => MPVersion::V3,
    }
}

/**
 * Get the corresponding `TemplateClasse` from the given string
 * 
 * Argument:
 * 
 * - `template` - Site template string.
 */
fn get_template_class(template: &str) -> TemplateClasse {
    match template {
        "MaximumSecurity" => TemplateClasse::MaximumSecurity,
        "Long" => TemplateClasse::Long,
        "Medium"=> TemplateClasse::Medium,
        "Short" => TemplateClasse::Short,
        "Basic" => TemplateClasse::Basic,
        "PIN" => TemplateClasse::PIN,
        "Name" => TemplateClasse::Name,
        "Phrase" => TemplateClasse::Phrase,
        _ => TemplateClasse::Long,
    }
}

/**
 * Generate the master key using `calculate_master_key` function.
 * 
 * Arguments:
 * 
 * - `user` - Name used for identification,
 * - `password` - The master password,
 * - `version_opt` - Version of the Master Password algorithm used.
 */
#[wasm_bindgen]
pub fn generate_master_key(user: &str, password: &str, version_opt: Option<usize>) -> Box<[u8]> {
    let version: usize = match version_opt {
        Some(version_value) => version_value,
        None => 3
    };

    calculate_master_key(user, password, get_mpversion(version)).to_vec().into_boxed_slice()
}

/**
 * Generate the site key and the password from the master key and the informations on the password identifier.
 * 
 * Arguments:
 * 
 * `master_key_vec` - The master key used to derive the site key,
 * `site_name` - Identifier used to generate the password (can be anything, not only a website name),
 * `site_counter` - Iteration of the password, used to provide an easy way for updating the password, 
 * `version_opt` - Version of the Master Password algorithm used to generate the password,
 * `template_opt` - The site template used to generate the password.
 */
#[wasm_bindgen]
pub fn generate_password_from_mk(master_key_vec: Box<[u8]>,
    site_name: &str,
    site_counter_opt: Option<u32>,
    version_opt: Option<usize>, 
    template_opt: Option<String>) -> String {

    let site_counter: u32 = match site_counter_opt {
        Some(site_counter_value) => site_counter_value,
        None => 1
    };
    
    let version: usize = match version_opt {
        Some(version_value) => version_value,
        None => 3
    };

    let template_string: String = match template_opt {
        Some(template_value) => template_value,
        None => "Long".to_owned()
    };

    let template: &str = &*template_string; // Using Deref<Target=str>
    
    let mpversion = get_mpversion(version);

    let master_key: [u8; 64] = {
        let mut mk: [u8; 64] = [0; 64];

        for i in 0..64 {
            mk[i] = master_key_vec[i];
        };

        mk
    };

    let site_key = calculate_site_key(master_key, 
        site_name, 
        site_counter, 
        KeyScope::Authentification, 
        mpversion
    );
            
    site_password(get_template_class(template), site_key, mpversion)
}

