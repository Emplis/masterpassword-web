function bindEventHandler() {
    // Login inputs
    let login_name      = document.getElementsByClassName("login-name-input")[0];
    let login_password  = document.getElementsByClassName("login-password-input")[0];

    // Password settings inputs
    let site_name_field = document.getElementsByClassName("site-name-input")[0];
    let counter_input   = document.getElementsByClassName("site-counter-input")[0];
    let version_input   = document.getElementsByClassName("algorithm-version-input")[0];
    let type_input      = document.getElementsByClassName("password-template-input")[0];

    // Password inputs
    let password_field  = document.getElementsByClassName("generated-password-input")[0];
    let pswd_visibility = document.querySelector(".password-visibility-btn > svg");
    let eye             = document.querySelector(".password-visibility-btn > svg > g > g[data-name='eye']");
    let eye_off         = document.querySelector(".password-visibility-btn > svg > g > g[data-name='eye-off']");

    // Functions used for events
    let login_inputs_update = (event) => {
        if (login_name.value == "" && login_password.value == "") {
            site_name_field.disabled = true;
            site_name_field.value   = "";
            password_field.value    = "";
        } else {
            if (site_name_field.disabled && (login_name.value != "" && login_password.value != "")) {
                site_name_field.disabled = false;
            }
            site_name_field.value = "";
            password_field.value = "";
            globalThis.discard_mpw = true;

            // Go to next input if `Enter` is pressed
            if (event.keyCode === 13) {
                if (login_name == document.activeElement) {
                    login_password.focus();
                } else if (login_password == document.activeElement) {
                    site_name_field.focus();
                }
            }
        }
    };

    let update_password = () => {
        let site_name       = site_name_field.value;
        let site_counter    = counter_input.value;
        let site_version    = version_input.value;
        let site_template   = type_input.value;

        if (site_name !== "") {
            let generated_password = globalThis.mpw_credentials.get_password(site_name, site_counter, site_template, site_version);
            password_field.value = generated_password;
        }
    };

    // Bind events to the elements
    login_name.addEventListener("keyup", login_inputs_update);
    login_password.addEventListener("keyup", login_inputs_update);

    site_name_field.addEventListener("keyup", () => {
        if (site_name_field.value == "") {
            password_field.value = "";
        } else {
            // Generate the password on site name update
            if (globalThis.first_mpw || globalThis.discard_mpw) {
                globalThis.mpw_credentials = new globalThis.MasterPassWord(login_name.value, login_password.value);

                globalThis.first_mpw    = false;
                globalThis.discard_mpw  = false;
            }

            update_password();
        }
    });

    counter_input.addEventListener("change", update_password);
    version_input.addEventListener("change", update_password);
    type_input.addEventListener("change", update_password);

    // Copy the password when the password field is clicked
    password_field.addEventListener("click", () => {
        let password = password_field.value;
        navigator.clipboard.writeText(password);
        password_field.blur();
    });

    // Select the text in the counter input when clicked
    counter_input.addEventListener("click", () => {
        counter_input.select();
    });

    // Manage password visibility with the eye button
    pswd_visibility.addEventListener("click", () => {
        if (password_field.type == "text") {
            eye.style.setProperty("opacity", "100%");
            eye_off.style.setProperty("opacity", "0%");

            password_field.type = "password";
        } else {
            eye.style.setProperty("opacity", "0%");
            eye_off.style.setProperty("opacity", "100%");

            password_field.type = "text";
        }

        pswd_visibility.blur();
    });
}
