import init, { generate_master_key, generate_password_from_mk } from "/wasm/pkg/masterpassword_wasm.js";

async function run() {
    await init();

    console.log("WASM successfully loaded.");

    /**
     * Simple Lazy class
     * 
     * Data are initialized only the first time they are requested.
     */
    class Lazy {
        /**
         * @param {Function} value_maker
         */
        constructor(value_maker) {
            this.value = null;
            this.value_maker = value_maker;
        }

        get() {
            if (!this.value) {
                console.log("Generating master key.");
                this.value = this.value_maker();
            }

            return this.value;
        }
    }

    /**
     * Class used to store the master key and generate password on demand
     */
    class MPW {
        /**
         * @param {string} name 
         * @param {string} password 
         */
        constructor(name, password) {
            this.master_key_v0 = new Lazy(() => generate_master_key(name, password, 0));
            this.master_key_v1 = new Lazy(() => generate_master_key(name, password, 1));
            this.master_key_v2 = new Lazy(() => generate_master_key(name, password, 2));
            this.master_key_v3 = new Lazy(() => generate_master_key(name, password, 3));
        }

        /**
         * Return the master key of the corresponding algorithm version.
         * 
         * @param {string} site_name 
         * @param {number} site_counter 
         * @param {string} site_template 
         * @param {number} version 
         */
        get_password(site_name, site_counter = 1, site_template = "Long", version = 3) {
            switch (Number(version)) {
                case 0:
                    return generate_password_from_mk(this.master_key_v0.get(), site_name, site_counter, 0, site_template);
                case 1:
                    return generate_password_from_mk(this.master_key_v1.get(), site_name, site_counter, 1, site_template);
                case 2:
                    return generate_password_from_mk(this.master_key_v2.get(), site_name, site_counter, 2, site_template);
                case 3:
                    return generate_password_from_mk(this.master_key_v3.get(), site_name, site_counter, 3, site_template);
                default:
                    return generate_password_from_mk(this.master_key_v3.get(), site_name, site_counter, 3, site_template);
            }
        }
    }

    // Export the MPW object to the window context
    globalThis.MasterPassWord   = MPW;
    globalThis.first_mpw        = true;
    globalThis.discard_mpw      = false;

    // Bind events only when the WASM is loaded to avoid missing function errors
    bindEventHandler();
}

run();