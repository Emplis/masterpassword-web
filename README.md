# masterpassword-web

## What's this?

This is a responsive web app for the [Master Password](https://masterpassword.app/) password manager. The [main algorithm](https://gitlab.com/Emplis/masterpassword-rs) is implemented in Rust and compiled for WebAssembly.

A version is hosted on my website [here](https://mpw.theobattrel.fr/).

## Thanks

- [Eva Icons](https://github.com/akveo/eva-icons) for the eye and eye-off icons
- [Raleway](https://github.com/theleagueof/raleway/) for the font of the website
- [Jura](https://github.com/ossobuffo/jura) for the passwords font

And thanks to @BlueGone for always being here to answer my questions ! 😁
